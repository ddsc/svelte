/** @typedef {typeof __propDef.props}  LinearProps */
/** @typedef {typeof __propDef.events}  LinearEvents */
/** @typedef {typeof __propDef.slots}  LinearSlots */
export default class Linear extends SvelteComponentTyped<{
    externalNumber?: number;
    labels?: string[];
    belows?: any[];
    aboves?: any[];
    caption?: string;
    numbered?: boolean;
    clickable?: boolean;
    vertical?: boolean;
    functions?: any[];
    action?: string;
    buttons?: any[];
    icons?: string[];
}, {
    list: CustomEvent<any>;
    listItem: CustomEvent<any>;
} & {
    [evt: string]: CustomEvent<any>;
}, {}> {
}
export type LinearProps = typeof __propDef.props;
export type LinearEvents = typeof __propDef.events;
export type LinearSlots = typeof __propDef.slots;
import { SvelteComponentTyped } from "svelte";
declare const __propDef: {
    props: {
        externalNumber?: number;
        labels?: string[];
        belows?: any[];
        aboves?: any[];
        caption?: string;
        numbered?: boolean;
        clickable?: boolean;
        vertical?: boolean;
        functions?: any[];
        action?: string;
        buttons?: any[];
        icons?: string[];
    };
    events: {
        list: CustomEvent<any>;
        listItem: CustomEvent<any>;
    } & {
        [evt: string]: CustomEvent<any>;
    };
    slots: {};
};
export {};
