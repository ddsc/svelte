/** @typedef {typeof __propDef.props}  CellProps */
/** @typedef {typeof __propDef.events}  CellEvents */
/** @typedef {typeof __propDef.slots}  CellSlots */
export default class Cell extends SvelteComponentTyped<{
    item?: {};
}, {
    [evt: string]: CustomEvent<any>;
}, {}> {
}
export type CellProps = typeof __propDef.props;
export type CellEvents = typeof __propDef.events;
export type CellSlots = typeof __propDef.slots;
import { SvelteComponentTyped } from "svelte";
declare const __propDef: {
    props: {
        item?: {};
    };
    events: {
        [evt: string]: CustomEvent<any>;
    };
    slots: {};
};
export {};
