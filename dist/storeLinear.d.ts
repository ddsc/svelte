export const list: import("svelte/store").Writable<any[]>;
export const writableArrays: import("svelte/store").Writable<any[][]>;
export function pop(myList: any): void;
export function push(myList: any, newItem: any): void;
export function enqueue(myList: any, newItem: any): void;
export function dequeue(myList: any): void;
export function deleteItem(myList: any, item: any): void;
export function swapIndex(myList: any, i: any, j: any): void;
export function writeAbove(myList: any, i: any, text: any): void;
export function writeBelow(myList: any, i: any, text: any): void;
export function disable(myList: any, i: any): void;
export function unDisable(myList: any, i: any): void;
