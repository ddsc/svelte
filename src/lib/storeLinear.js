import { writable } from 'svelte/store';

export const list = writable([]);

export const writableArrays = writable([Array(),Array(),Array()]);

export const pop = function(myList){
	myList.splice(0,1);
}

export const push = function(myList,newItem){
	myList.splice(0,0,newItem);
}

export const enqueue = function(myList,newItem){
	myList.splice(0,0,newItem);
}

export const dequeue = function(myList){
	myList.splice(myList.length-1,1);
}

export const deleteItem = function (myList,item) {
	var i = myList.indexOf(item);
	myList.splice(i,1);
}

export const swapIndex = function(myList,i,j){
	var item1 = myList[i];
	var item2 = myList[j];
	if(i > j){
		myList.splice(i,1,item2);
		myList.splice(j,1,item1);
	}else{
		myList.splice(j,1,item1);
		myList.splice(i,1,item2);			
	}
}

export const writeAbove = function(myList,i,text){
	myList[i].above = text;
}

export const writeBelow = function(myList,i,text){
	myList[i].below = text;
}

export const disable = function(myList,i){
	myList[i].disabled = true;
}
export const unDisable = function(myList,i){
	myList[i].disabled = false;
}